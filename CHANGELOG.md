# Changelog

## [1.8] - 2020-6-12
- Added a variable (cler_use_first_client_as_administrator_screen) to easily change the administrator screen to be the first one in the clients' table (=1) or the last one (=0)
- Recreated the save and change Chat Buttons as they were requiring all input fields to be not empty.
- Changed Name Admin-Override to nothing. It was intended to show that the chat boxes were not opened by a message of the experimenter on the client screens.
- Relocated the chat bar to the bottom 
- Now supporting zTree v3.5.0 onwards. Changed boolean variables as they were introduced in zTree v4.0.1 and not using any for-loops
- Separated the header boxes as multiple header boxes were introduced in zTree 4.0.22

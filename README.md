# cler_communicator v1.8

## Purpose

A chat between experimenter and subjects (for z-Tree[^2]).

## z-Tree compatibility and supported screen resolution

- Tested and working with z-Tree Version 3.5.0 and upwards up to the latest version (4.1.11) and probably beyond.
- Displays very well on each resolution starting at 800x600.

## Feature list

- Everything is implemented within z-Tree[^2]
  - It automatically opens a message box on their screens and close it after a specifiable time period.
  - You may select whether to send a message to all participants or just a specific subject.
  - Users might only message the experimenter, preventing communication between subjects.
- Messages are stored in your z-Tree output (and the .gsf file, but not on an external server) 
- Messages are sent by just entering the content and pressing enter.

## Example

Using *[cler_communicator](https://gitlab.com/cler1/cler_communicator)* along [z-Tree unleashed](https://cler1.gitlab.io/ztree-unleashed-doc/docs/home/) gives an easy way of 
writing messages to subjects while ensuring that they (a) cannot communicate between each other and (b) don't have to switch to
another website and leave the experiment.

## Set-up
1. Import cler_communicator_v1.8.txt via `File > Import...`

2. Installing the `cler_communicator` is straightforward. You just need to copy the table `cler_chattable` (1) and the `global` program (2) 
to the background of your own z-Tree experiment. Finally, you need to copy the container `cler_communicator_v1.8` (3) to the either the background active screen if you want 
to use it during the whole time of your experiment. In case you want to use it only in specific stages (e.g. a waiting room before the experiment) you just need copy the 
the container `cler_communicator_v1.8` to the active screens of these stages. The cler_communicator is even going to display all messages ever sent even if you decide not to
display it in each stage. 

![You just need to copy these parts.](images/How_to_install_the_cler_communicator.png)

## License

*cler_communicator* is licensed under the MIT License. Please read `LICENSE` for more information.

## FAQ

**Do I have to cite anything if I use this program?**

No! But remember to cite z-Tree[^2] or z-Tree unleashed[^3] in case you use them for your research.

**z-Tree crashes after I've imported your txt file and adjusted the number of subjects**

In this case please first save your treatment to a .ztt file and load it. This should prevent a z-Tree crash. 
I guess the issue is that the imported z-Tree file sets a number of participants (set to 6 while I've exported it) and 
expects the same number of participants. But as this number was changed z-Tree crashes...


## Showcase
Here are some screenshots of the lastest version showing the [cler_communicator](https://gitlab.com/cler1/cler_communicator) in action.

**1.** The administrator (or experimenter) is sending a message to all participants. A notification shows that a message is displayed to the participants for a specific time.

![Administrator screen.](images/cler_communicator_v1_8_screenshots/0Administrator_Message_sent.PNG)

**2.** The message box on the (specific) participants' screen is opened automatically for a specifiable time and a customizable (and even easily deactivatable) message is shown. 

![Participants' screen.](images/cler_communicator_v1_8_screenshots/1clientscreen.PNG)

**3.** *cler_communicator* is intended to work as a communication channel between the experimenter and the participants. Participants may not communicate with each other. If you want this functionality, you may just use the vanilla built-in chat functionality of z-Tree[^2].

![Communication Channel.](images/cler_communicator_v1_8_screenshots/2clientscreen_message_to_experimenter.PNG)

**4.** The participants may only open and close the chat box. Furthermore the experimenter may deactivate it during the experiment by using the `Disable Chat` Button.

![Chat states](images/cler_communicator_v1_8_screenshots/3Screenstates.PNG.PNG)

**5.** The experimenter may choose who is receiving his messages (1) all or (2) only a specific participant. We implemented a scrollbar which makes selecting the participant easy. (Entering a number is not possible to limitiations set by z-Tree.) 

![Choose recipient](images/cler_communicator_v1_8_screenshots/4Experimenter_choose_receipient.PNG)


## References

[^2]: Fischbacher, U. (2007). "z-Tree: Zurich toolbox for ready-made economic experiments". Experimental economics, 10(2), pp. 171–178.
[^3]: Duch, Matthias & Grossmann, Max & Lauer, Thomas. (2020). "z-Tree unleashed: A novel client-integrating architecture for conducting z-Tree experiments over the Internet." 